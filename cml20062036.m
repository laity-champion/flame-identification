function varargout = cml20062036(varargin)
%CML20062036 M-file for cml20062036.fig
%      CML20062036, by itself, creates a new CML20062036 or raises the existing
%      singleton*.
%
%      H = CML20062036 returns the handle to a new CML20062036 or the handle to
%      the existing singleton*.
%
%      CML20062036('Property','Value',...) creates a new CML20062036 using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to cml20062036_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      CML20062036('CALLBACK') and CML20062036('CALLBACK',hObject,...) call the
%      local function named CALLBACK in CML20062036.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cml20062036

% Last Modified by GUIDE v2.5 06-May-2020 15:38:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cml20062036_OpeningFcn, ...
                   'gui_OutputFcn',  @cml20062036_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cml20062036 is made visible.
function cml20062036_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for cml20062036
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cml20062036 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cml20062036_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_5_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_6_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_31_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
[X,map]=rgb2ind(I,0.1);
nbcol=size(map,1);
colormap(pink(nbcol));
cod_X=wcodemat(X,nbcol);
[ca1,ch1,cv1,cd1]=dwt2(X,'db1');
cod_ca1=wcodemat(ca1,nbcol);
cod_ch1=wcodemat(ch1,nbcol);
cod_cv1=wcodemat(cv1,nbcol);
cod_cd1=wcodemat(cd1,nbcol);
subplot(1,1,1),image([cod_ca1,cod_ch1;cod_cv1,cod_cd1]);title('对cats索引图像的1次小波分解')


% --------------------------------------------------------------------
function Untitled_32_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
[X,map]=rgb2ind(I,0.1);
nbcol=size(map,1);
colormap(pink(nbcol));
cod_X=wcodemat(X,nbcol);
[ca1,ch1,cv1,cd1]=dwt2(X,'db1');
[ca2,ch2,cv2,cd2]=dwt2(ca1,'db1');
cod_ca2=wcodemat(ca2,nbcol);
cod_ch2=wcodemat(ch2,nbcol);
cod_cv2=wcodemat(cv2,nbcol);
cod_cd2=wcodemat(cd2,nbcol);
subplot(1,1,1),image([cod_ca2,cod_ch2;cod_cv2,cod_cd2])
title('对cats索引图像的2次小波分解')


% --------------------------------------------------------------------
function Untitled_33_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
[X,map]=rgb2ind(I,0.1);
nbcol=size(map,1);
colormap(pink(nbcol));
cod_X=wcodemat(X,nbcol);
[ca1,ch1,cv1,cd1]=dwt2(X,'db1');
a0=idwt2(ca1,ch1,cv1,cd1,'db1',size(X));
a0=wcodemat(a0,nbcol);
subplot(1,1,1),image(a0)
title('对cats索引图像的第一层重构')


% --------------------------------------------------------------------
function Untitled_34_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
[X,map]=rgb2ind(I,0.1);
nbcol=size(map,1);
colormap(pink(nbcol));
cod_X=wcodemat(X,nbcol);
[ca1,ch1,cv1,cd1]=dwt2(X,'db1');
[ca2,ch2,cv2,cd2]=dwt2(ca1,'db1');
a0=idwt2(ca2,ch2,cv2,cd2,'db1',size(X));
a0=wcodemat(a0,nbcol);
subplot(1,1,1),image(a0)
title('对cats索引图像的第二层重构')



% --------------------------------------------------------------------
function Untitled_25_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I=imread('cats.jpg');
V=0.02;
noise1=imnoise(I,'gaussian',0,V);%加高斯噪声
subplot(2,2,1),imshow(noise1);title('加高斯噪声')
noise=0.1*randn(size(I));%加随机噪声
noise2=imadd(I,im2uint8(noise));
subplot(2,2,2),imshow(noise2);title('加随机噪声')
noise3=imnoise(I,'salt & pepper', 0.02);
subplot(2,2,3),imshow(noise3);title('加椒盐噪声')
noise4=imnoise(I,'speckle',0.06);
subplot(2,2,4),imshow(noise4);title('加乘性噪声')
%I=imread('cameraman.tif');
%J=imnoise(I,'salt & pepper',0.02);
%subplot(121),imshow(J);title
%K=medfilt2(J);%使用3*3的邻域窗的中值滤波
%subplot(122),imshow(K);title


% --------------------------------------------------------------------
function Untitled_26_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I=imread('gray.bmp');
I1=imnoise(I,'salt & pepper',0.06);
I2=double(I1)/255;
J2=medfilt2(I2,[3 3]);
subplot(1,2,1),imshow(I1);title('加椒盐噪声后的图像')
subplot(1,2,2),imshow(J2);title('中值滤波后的图像')
%[X,map]=imread('trees.tif');
%gmap=rgb2gray(map);
%figure,imshow(X,map);
%figure,imshow(X,gmap);

% --------------------------------------------------------------------
function Untitled_27_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_28_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc
clear
A=imread('gray.bmp')
I=double(A)/255;
%load woman2
%I=ind2gray(X,map)
noisy=imnoise(I,'gaussian',0.06);
[M N]=size(I);
F=fft2(noisy);
fftshift(F);
Dcut=100;
D0=150;
D1=250;
for u=1:M
    for v=1:N
        D(u,v)=sqrt(u^2+v^2);
        BUTTERH(u,v)=1/(1+(sqrt(2)-1)*(D(u,v)/Dcut)^2);
        EXPOTH(u,v)=exp(log(1/sqrt(2))*(D(u,v)/Dcut)^2);
        if D(u,v)<D0
            TRAPEH(u,v)=1;
        else if D(u,v)<=D1
                TRAPEH(u,v)=(D(u,v)-D1)/(D0-D1);
            else TRAPEH(u,v)=0;
            end
        end
    end
end
BUTTERG=BUTTERH.*F;
BUTTERfiltered=ifft2(BUTTERG);
EXPOTG=EXPOTH.*F;
EXPOTfiltered=ifft2(EXPOTG);
TRAPEG=TRAPEH.*F;
TRAPEfiltered=ifft2(TRAPEG);
subplot(2,2,1),imshow(noisy);title('加入高斯噪声的cats灰度图像');
subplot(2,2,2),imshow(BUTTERfiltered);title('经过巴特沃斯低通滤波器后的图像');
subplot(2,2,3),imshow(EXPOTfiltered);title('经过指数低通滤波器后的图像');
subplot(2,2,4),imshow(TRAPEfiltered);title('经过梯形低通滤波器后的图像');
%function av=average(A);
%A=imread('cats.jpg');
%if ndims(A)>2
 % error('the dimensions of the input cannot exceed ');
%end




% --------------------------------------------------------------------
function Untitled_29_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_30_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
I=imread('gray.bmp');
subplot(2,2,1),imshow(I);title('原始cats灰度图像');
Ic=imcomplement(I);
BW=im2bw(Ic,graythresh(I));
subplot(2,2,2),imshow(BW);title('阈值截取分割后图像');
se=strel('disk',10);
BWc=imclose(BW,se);
BWco=imopen(BWc,se);
subplot(2,2,3),imshow(BWco);title('对小图像进行删除后图像');
mask=BWc&BWco;
subplot(2,2,4),imshow(mask);title('检测结果的图像');
A=imread('cats.jpg');
B=sum(A(:))/length(A(:));%计算平均亮度
%C=max(I);
if B>10
 open('test.fig');
end
%h=guihandles;
%set(h.text3,'string',num2str(a1+a2));
% --------------------------------------------------------------------
function Untitled_21_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
gray=rgb2gray(I);
J=histeq(gray);
subplot(2,2,1),imshow(gray);title('原始cats灰度图像');
subplot(2,2,2),imshow(J);title('直方图均衡化后的cats灰度图像');
subplot(2,2,3),imhist(gray);title('原始cats灰度图像直方图');
subplot(2,2,4),imhist(J);title('直方图均衡化后的cats灰度图像直方图');


% --------------------------------------------------------------------
function Untitled_22_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg')
gray=rgb2gray(I);
subplot(1,2,1),imshow(gray)
title('原始cats灰度图像')
background=imopen(gray,strel('disk',10))
I2=imsubtract(gray,background);
subplot(1,2,2),imshow(I2)
title('减去背景后的图像')


% --------------------------------------------------------------------
function Untitled_23_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
level=graythresh(I);
bw=im2bw(I,level);
subplot(1,2,1),imshow(I);title('原始cats图像');
subplot(1,2,2),imshow(bw);title('阈值化后的cats图像')


% --------------------------------------------------------------------
function Untitled_24_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
gray=rgb2gray(I);
J=imadjust(gray,[0.3,0.7],[]);
subplot(2,2,1),imshow(gray);title('原始cats灰度图像');
subplot(2,2,2),imshow(J);title('调整对比度后的cats灰度图像');
subplot(2,2,3),imhist(gray);title('原始cats灰度图像直方图');
subplot(2,2,4),imhist(J);title('    调整对比度后的cats灰度图像直方图');




% --------------------------------------------------------------------
function Untitled_15_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
subplot(1,1,1),imshow(I);title('请用鼠标选择任意几个像素点后按回车以显示所选像素点的数据值！');
vals =impixel


% --------------------------------------------------------------------
function Untitled_16_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
subplot(1,1,1),imshow cats.jpg 
title('请用鼠标选择一线段后按回车以显示轨迹强度图！')
improfile



% --------------------------------------------------------------------
function Untitled_17_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear   
I=imread('gray.bmp');
subplot(1,1,1),imshow(I);title('原始cats灰度图像')
figure(1),
f1=figure(1);
set(f1,'NumberTitle','off','Name','图像轮廓图')   
imcontour(I)       



% --------------------------------------------------------------------
function Untitled_18_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('gray.bmp');
subplot(2,1,1),imshow(I);title('cats灰度图像')
subplot(2,1,2),imhist(I,64);title('cats灰度图像的直方图')
      



% --------------------------------------------------------------------
function Untitled_19_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_10_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg'); 
subplot(2,2,1),imshow(I);title('原始图像')
X1=imresize(I,0.1,'nearest');
subplot(2,2,2),imshow(X1,[]);title('最近邻插值法实现图像缩放')
X2=imresize(I,0.1,'bilinear');
subplot(2,2,3),imshow(X2,[]);title('双线性插值法实现图像缩放')
X3=imresize(I,0.1,'bicubic');
subplot(2,2,4),imshow(X3,[]);title('双立方插值法实现图像缩放')




% --------------------------------------------------------------------
function Untitled_11_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear    
I=imread('cats.jpg');
J=imrotate(I,35, 'bilinear');
subplot(1,2,1),imshow(I);title('原始图像')
subplot(1,2,2),imshow(J);title('逆时针旋转35°图像')


% --------------------------------------------------------------------
function Untitled_13_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
%subplot(1,1,1),imshow(I);title('请选择要裁剪的区域，并双击选定区域以显示');
msgbox('请选择要裁剪的区域，并双击选定区域以显示','提示信息');
waitforbuttonpress;
clf;
I2=imcrop(I);
close
subplot(1,2,1),imshow(I);title('原始cats——RGB图像');
subplot(1,2,2),imshow(I2);title('裁剪后的cats——RGB图像');

% --------------------------------------------------------------------
function Untitled_14_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
transform(I);
I=imread('gray.bmp');
transform(I);
load trees
transform(X,map);
% --------------------------------------------------------------------
function Untitled_7_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
subplot(1,1,1),imshow(I);title('cats——RGB图像');


% --------------------------------------------------------------------
function Untitled_8_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
gray=rgb2gray(I);
imwrite(gray,'gray.bmp');
level=graythresh(I);
bw=im2bw(I,level);
imwrite(bw,'bw.bmp');
[ind,map]=gray2ind(gray,64);
save ind
msgbox('已保存为灰度图像gray.bmp、索引图像ind.mat、二值图像bw.bmp！','提示信息');

% --------------------------------------------------------------------
function Untitled_9_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
close all
exit(0)


% --------------------------------------------------------------------
function Untitled_35_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
[x,y,z]=cylinder;
subplot(1,2,1),warp(x,y,z,I);title('圆柱形纹理映射');
[x,y,z]=sphere;
 subplot(1,2,2),warp(x,y,z,I);title('球形纹理映射');


% --------------------------------------------------------------------
function Untitled_36_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('cats.jpg');
gray=rgb2gray(I);
J=histeq(gray,32);
[counts,x]=imhist(J);
I=imread('cats.jpg');
Q=rgb2gray(I);
subplot(2,2,1),imshow(Q);title('原始cats灰度图像');
subplot(2,2,3),imhist(Q);title('原始cats灰度图像的直方图');
M=histeq(Q,counts);
subplot(2,2,2),imshow(M);title('直方图规定化后的cats灰度图像');
subplot(2,2,4),imhist(M);title('直方图均衡化成32个灰度级后的cats灰度图像直方图');
axis square


% --------------------------------------------------------------------
function Untitled_37_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_40_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_41_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('gray.bmp');
BW1=edge(I,'sobel');
BW2=edge(I,'canny');
BW3=edge(I,'prewitt');
BW4=edge(I,'log');
subplot(3,2,1),imshow(I);title('原始cats灰度图像');
subplot(3,2,3),imshow(BW1);title('Sobel边缘检测');
subplot(3,2,4),imshow(BW2);title('Canny边缘检测');
subplot(3,2,5),imshow(BW2);title('prewitt边缘检测');
subplot(3,2,6),imshow(BW2);title('log边缘检测');



% --------------------------------------------------------------------
function Untitled_42_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
[I,map]=imread('gray.bmp');
subplot(3,2,1),imshow(I,map);title('用梯度法实现图像锐化');
I=double(I);
[IX,IY]=gradient(I);
GM=sqrt(IX.*IX+IY.*IY);
OUT1=GM;
subplot(3,2,2),imshow(OUT1,map);
OUT2=I;
J=find(GM>=10);
OUT2(J)=GM(J);
subplot(3,2,3),imshow(OUT2,map);
OUT3=I;
J=find(GM>=10);
OUT3(J)=255;
subplot(3,2,4),imshow(OUT3,map);
OUT4=I;
J=find(GM<=10);
OUT4(J)=255;
subplot(3,2,5),imshow(OUT4,map);
OUT5=I;
J=find(GM>=10);
OUT5(J)=255;
Q=find(GM<10);
OUT5(Q)=0;
subplot(3,2,6),imshow(OUT5,map);


% --------------------------------------------------------------------
function Untitled_39_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%对随机噪声用deconvwnr函数复原
I=imread('cats.jpg');
LEN=31;
THETA=11;
PSF=fspecial('motion',LEN,THETA);
Blurred=imfilter(I,PSF,'circular','conv');
%wnr1=deconvwnr(Blurred,PSF);
noise2=0.1*randn(size(I));%加随机噪声
noise3=imadd(I,im2uint8(noise2));
subplot(1,2,1),imshow(noise3);title('加随机噪声');
wnr2=deconvwnr(Blurred,PSF);
subplot(1,2,2),imshow(wnr2);title('用deconvwnr函数复原');


% --------------------------------------------------------------------
function Untitled_44_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('gray.bmp');
H=fspecial('sobel');
subplot(1,2,1),imshow(I);title('原始cats灰度图像');
J=filter2(H,I);
subplot(1,2,2),imshow(J);title('Sobel算子对图像锐化结果');


% --------------------------------------------------------------------
function Untitled_45_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('gray.bmp');
I=double(I);
subplot(1,2,1),imshow(I,[]);title('原始cats灰度图像');
h=[0 1 0,1 -4 1,0 1 0];
J=conv2(I,h,'same');
K=I-J;
subplot(1,2,2),imshow(K,[]);title('拉氏算子对模糊图像进行增强')


% --------------------------------------------------------------------
function Untitled_46_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear
I=imread('gray.bmp');
J=im2double(I);
subplot(2,2,1),imshow(J,[]);title('原始cats灰度图像');
h2=[-1 -1 -1,-1 9 -1,-1 -1 -1];
h3=[1 -2 1,-2 5 -2,1 -2 1];
h4=1/7.*[-1 -2 -1, -2 19 -2,-1 -2 -1];
A=conv2(J,h2,'same');
subplot(2,2,2),imshow(A,[]);title('H2算子滤波结果');
B=conv2(J,h3,'same');
subplot(2,2,3),imshow(B,[]);title('H3算子滤波结果');
C=conv2(J,h4,'same');
subplot(2,2,4),imshow(C,[]);title('H4算子滤波结果');


% --------------------------------------------------------------------
function Untitled_47_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[I,map]=imread('gray.bmp');
noisy=imnoise(I,'gaussian',0.01);
[M N]=size(I);
F=fft2(noisy);
fftshift(F);
Dcut=100;
D0=250;
D1=150;
for u=1:M
    for v=1:N
        D(u,v)=sqrt(u^2+v^2);
        BUTTERH(u,v)=1/(1+(sqrt(2)-1)*(Dcut/D(u,v))^2);
        EXPOTH(u,v)=exp(log(1/sqrt(2))*(Dcut/D(u,v))^2);
        if D(u,v)<D1
            THPFH(u,v)=0;
        else if D(u,v)<=D0
                THPFH(u,v)=(D(u,v)-D1)/(D0-D1);
            else THPFH(u,v)=1;
            end
        end
    end
end
    BUTTERG=BUTTERH.*F;
    BUTTERfiltered=ifft2(BUTTERG);
    EXPOTG=EXPOTH.*F;
    EXPOTfiltered=ifft2(EXPOTG);
    THPFG=THPFH.*F;
    THPFfiltered=ifft(THPFG);
    subplot(2,2,1),imshow(noisy);title('加入高斯噪声的cats灰度图像');
    subplot(2,2,2),imshow(BUTTERfiltered);title('经过巴特沃斯高通滤波器后的图像');
    subplot(2,2,3),imshow(EXPOTfiltered);title('经过指数高通滤波器后的图像');
    subplot(2,2,4),imshow(THPFfiltered);title('经过梯形高通滤波器后的图像');


% --- Executes during object creation, after setting all properties.
function text5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over text7.
function text7_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to text7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
