function []=transform(I,map)
if  isrgb(I)
    figure(1),f1=figure;
    f1=figure(1);
    set(f1,'NumberTitle','off','Name','将RGB图像转换为其他格式')   
    subplot(2,2,1),subimage(I);title('原始RGB图像');
    gray=rgb2gray(I);
    subplot(2,2,2),subimage(gray);title('转换为灰度图像');
    [ind,map]=rgb2ind(I,0.5);
    subplot(2,2,3),subimage(ind,map);title('转换为索引图像');
    bw=im2bw(I,0.1);
    subplot(2,2,4),subimage(bw);title('转换为二值图像');
else if isgray(I)
    figure(2),title('对灰度图像的格式转换');
    f1=figure(2);
    set(f1,'NumberTitle','off','Name','将灰度图像转换为其他格式') 
    subplot(2,2,1),subimage(I);title('原始灰度图像');
    [ind,map]=gray2ind(I,64);
    subplot(2,2,2),subimage(ind,map);title('转换为索引图像');
    RGB=ind2rgb(ind,map);    
    subplot(2,2,3),subimage(RGB);title('转换为RGB图像');
    bw=im2bw(I,0.5);
    subplot(2,2,4),subimage(bw);title('转换为二值图像');
    else if isind(I)
            figure(3)
            f1=figure(3);
            set(f1,'NumberTitle','off','Name','将索引图像换为其他格式') 
            subplot(2,2,1),subimage(I,map);title('原始索引图像'); 
            RGB=ind2rgb(I,map);
            subplot(2,2,2),subimage(RGB);title('转换为RGB图像');
            gray=rgb2gray(RGB);
            subplot(2,2,3),subimage(gray);title('转换为灰度图像');
            bw=im2bw(I,map,0.6);
            subplot(2,2,4),subimage(bw);title('转换为二值图像');
        end
    end
end
